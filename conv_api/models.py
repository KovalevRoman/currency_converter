from django.db import models

# Create your models here.
class Course(models.Model):
    czk_eur = models.FloatField()
    pln_usd = models.FloatField()
    eur_pln = models.FloatField()
    eur_czk = models.FloatField()
    czk_usd = models.FloatField()
    usd_eur = models.FloatField()
    pln_czk = models.FloatField()
    eur_usd = models.FloatField()
    pln_eur = models.FloatField()
    usd_czk = models.FloatField()
    czk_pln = models.FloatField()
    usd_pln = models.FloatField()

    updated = models.TimeField()