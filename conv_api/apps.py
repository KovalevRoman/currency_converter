from django.apps import AppConfig


class ConvApiConfig(AppConfig):
    name = 'conv_api'
