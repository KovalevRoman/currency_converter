from django.shortcuts import render
from django.views.generic import TemplateView
from rest_framework.generics import ListAPIView
from .serializer import ActionSerialiser
from .models import Course
# Create your views here.


class RetrieveConverterView(ListAPIView):
    serializer_class = ActionSerialiser
    lookup_field = 'pk'

    def get_queryset(self):
        return Course.objects.all()


class ConverterView(TemplateView):
    template_name = 'converter.html'