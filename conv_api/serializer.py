from rest_framework.serializers import ModelSerializer
from .models import Course

class ActionSerialiser(ModelSerializer):
    class Meta:
        model = Course
        fields = [
        'czk_eur',
        'czk_pln',
        'czk_usd',
        'eur_pln',
        'eur_usd',
        'pln_usd',
        'eur_czk',
        'pln_czk',
        'usd_czk',
        'pln_eur',
        'usd_eur',
        'usd_pln',
        ]