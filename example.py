'''run with crone'''

import requests
import json
import sqlite3
import datetime

APP_ID = 'c4a44771bc6a40838265616922fe9e47'
URL = 'https://openexchangerates.org/api/latest.json?app_id=c4a44771bc6a40838265616922fe9e47'
curlist = {
    'CZK': 'CZK',
    'PLN': 'PLN',
    'USD': 'USD',
    'EUR': 'EUR',
}


def main():
    courses = {}
    s = requests.get(URL).text
    data = json.loads(s)['rates']
    courses.update(
        {
        'czk_eur': round(data['EUR']/data['CZK'], 3),
        'czk_pln': round(data['PLN']/data['CZK'], 3),
        'czk_usd': round(data['CZK'], 3),
        'eur_pln': round(data['PLN']/data['EUR'], 3),
        'eur_usd': round(1/data['EUR'], 3),
        'pln_usd': round(1/data['PLN'], 3),
        'eur_czk' : round(data['CZK']/data['EUR'], 3),
        'pln_czk': round(data['CZK']/data['PLN'], 3),
        'usd_czk': round(data['CZK'], 3),
        'pln_eur': round(data['EUR']/data['PLN'], 3),
        'usd_eur': round(data['EUR'], 3),
        'usd_pln': round(data['PLN'], 3),
    })
    s = ''
    s = "'" + \
    str(courses['czk_eur']) + "','" + str(courses['pln_usd']) + "','" + str(courses['eur_pln']) + "','" + \
    str(courses['eur_czk']) + "','" + str(courses['czk_usd']) + "','" + str(courses['usd_eur']) + "','" + \
    str(courses['pln_czk']) + "','" + str(courses['eur_usd']) + "','" + str(courses['pln_eur']) + "','" + \
    str(courses['usd_czk']) + "','" + str(courses['czk_pln']) + "','" + str(courses['usd_pln']) + "'"
    a = "insert into conv_api_course (id, czk_eur, pln_usd, eur_pln, eur_czk, czk_usd, usd_eur, pln_czk, eur_usd, pln_eur, \
    usd_czk, czk_pln, usd_pln, updated) values ('1', " + s + ",'" + str(datetime.datetime.now()) + "')"
    conn = sqlite3.connect('db.sqlite3')
    cursor = conn.cursor()
    cursor.execute("DELETE FROM conv_api_course")
    conn.commit()
    cursor.execute(a)
    conn.commit()
    # names = list(map(lambda x: x[0], cursor.description))
    # print(names)
    conn.close()

if __name__ == '__main__':
    main()


#       usd\euro = 0.81   -> usd = 0.81*eur
#       usd\czk = 20,66    -> 0.81*eur = 20.66*ch -> eur\ch = 20,66\0.81
#       eur\czk = (usd\czk)\(usd\euro)
#     czk_euro = (usd/eur)/(usd/czk)
#     czk_pln =  (usd/pln)/(usd/czk)
#     czk_usd =  (usd/usd)/(usd/czk)
#     euro_pln =  (usd/pln)/(usd/euro)
#     euro_usd =  (usd/usd)/(usd/euro)
#     pln_usd =  (usd/usd)/(usd/pln)
